# Debian Distribution Build
### Introduction to Distro Build
#### Requriments :-
- Understand [File System Hierarchy](https://www.linux.com/tutorials/linux-filesystem-explained/)
- Basic Linux System Administration
  - [Link 1](https://opensourceforu.com/2016/07/introduction-linux-system-administration/)
  - [Link 2](https://opensource.com/article/17/7/20-sysadmin-commands)

#### Prepare Environment :-
- Install [live-build](https://live-team.pages.debian.net/live-manual/html/live-manual/installation.en.html#124)
- Install [VirtualBox](https://tecadmin.net/install-virtualbox-debian-9-stretch/)

#### Tasks :-
- GRUB
  - Understand GRUB and GRUB configuration file
  - How to edit _grub.cfg_
- Plymouth
  - Understand Plymouth themes
  - Custom bootsplash
- Shell Scripting
  - Understand how shell script works
  - shell scripting to customize the OS
- Applications
  - List of Applications to be installed
  - Check for Dependencies
- Design
  - Wallpaper Design
  - Icons Design
  - Bootsplash Design
  - GRUB Image Design

#### Introduction to live-build :-
- Understand how live-build works
  - `lb config`
  - `lb build`
  - `lb clean`
- Understand the skeleton of the _config_ directory
- Here is the manual page for [live-build](https://live-team.pages.debian.net/live-manual/html/live-manual/index.en.html)
